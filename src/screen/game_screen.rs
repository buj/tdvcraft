pub mod world;

use sfml::{window::Key, graphics::{RenderTarget, Color, RenderStates, Sprite, Transformable, RenderWindow}, system::Vector2f};

use crate::Result;

use self::world::{World, EntityType, Entity};

use super::{Screen, Flow, Context};

/// Scale for scaling the map
pub const SCALE: f32 = 4.0;
/// Player speed (block/sec)
pub const PLAYER_SPEED: f32 = 2.0;

/// Screen for the game itself
pub struct GameScreen {
    /// World screen works with
    world: World,
    /// Pressed keys
    keys: (
        (bool, bool),
        (bool, bool),
        (bool, bool),
        (bool, bool),
    ),
    /// Mouse
    mouse: (i32, i32, bool),
    /// Resize surface to (sfml, why)
    resize_to: Option<(u32, u32)>,
}

impl GameScreen {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for GameScreen {
    fn default() -> Self {
        let mut world = World::new();
        world.spawn(Entity { x: 0.0, y: 0.0, entity: EntityType::Player});

        GameScreen {
            world,
            keys: Default::default(),
            mouse: Default::default(),
            resize_to: None,
        }
    }
}

impl Screen for GameScreen {
    fn handle_event(&mut self, event: &sfml::window::Event, _: Context) -> Result<Flow> {
        match event {
            sfml::window::Event::Closed => Ok(Flow::Quit),
            sfml::window::Event::KeyPressed { code: Key::Escape, .. } => Ok(Flow::Quit),
            sfml::window::Event::Resized { width, height } => {
                self.resize_to = Some((*width, *height));

                Ok(Flow::Continue)
            }
            sfml::window::Event::KeyPressed { code, ..  } => {
                match code {
                    Key::Up => self.keys.0.0 = true,
                    Key::W => self.keys.0.1 = true,

                    Key::Left => self.keys.1.0 = true,
                    Key::A => self.keys.1.1 = true,

                    Key::Down => self.keys.2.0 = true,
                    Key::S => self.keys.2.1 = true,

                    Key::Right => self.keys.3.0 = true,
                    Key::D => self.keys.3.1 = true,

                    _ => (),
                }
                Ok(Flow::Continue)
            }
            sfml::window::Event::KeyReleased { code, ..  } => {
                match code {
                    Key::Up => self.keys.0.0 = false,
                    Key::W => self.keys.0.1 = false,

                    Key::Left => self.keys.1.0 = false,
                    Key::A => self.keys.1.1 = false,

                    Key::Down => self.keys.2.0 = false,
                    Key::S => self.keys.2.1 = false,

                    Key::Right => self.keys.3.0 = false,
                    Key::D => self.keys.3.1 = false,

                    _ => (),
                }
                Ok(Flow::Continue)
            }
            sfml::window::Event::MouseButtonPressed { x, y, button } => {
                match button {
                    sfml::window::mouse::Button::Left => (),
                    _ => return Ok(Flow::Continue),
                }
                self.mouse.0 = *x;
                self.mouse.1 = *y;
                self.mouse.2 = true;
                Ok(Flow::Continue)
            }
            sfml::window::Event::MouseButtonReleased { x, y, button } => {
                match button {
                    sfml::window::mouse::Button::Left => (),
                    _ => return Ok(Flow::Continue),
                }
                self.mouse.0 = *x;
                self.mouse.1 = *y;
                self.mouse.2 = false;
                Ok(Flow::Continue)
            }
            _ => Ok(Flow::Continue),
        }
    }

    fn frame(&mut self, render_target: &mut RenderWindow, context: Context) -> Result<Flow> {
        if let Some((x, y)) = self.resize_to {
            self.resize_to = None;
            // TODO: Resize window (sfml, why?)
            let _ = (x, y);
        }

        let player_coords = {
            match self.world.entities.iter_mut().find(|x| x.entity == EntityType::Player) {
                Some(x) => {
                    if self.keys.0.0 || self.keys.0.1 {
                        x.y += context.delta.as_secs_f32() * PLAYER_SPEED;
                    }
                    if self.keys.1.0 || self.keys.1.1 {
                        x.x += context.delta.as_secs_f32() * PLAYER_SPEED;
                    }
                    if self.keys.2.0 || self.keys.2.1 {
                        x.y -= context.delta.as_secs_f32() * PLAYER_SPEED;
                    }
                    if self.keys.3.0 || self.keys.3.1 {
                        x.x -= context.delta.as_secs_f32() * PLAYER_SPEED;
                    }
                    (x.x, x.y)
                }
                None => return Ok(Flow::Continue),
            }
        };

        let (shift_x, shift_y) = {
            let (shift_x, shift_y) = (player_coords.0.fract(), player_coords.1.fract());
            let (shift_x, shift_y) = (
                if shift_x.is_sign_negative() {
                    1.0 + shift_x
                } else { shift_x },
                if shift_y.is_sign_negative() {
                    1.0 + shift_y
                } else { shift_y },
            );
            ((shift_x * 16.0 * SCALE) as i32, (shift_y * 16.0 * SCALE) as i32)
        };

        render_target.clear(Color::BLACK);
        let (base_x, base_y) = {
            let size = render_target.size();
            let (x, y) = (size.x as f32, size.y as f32);
            (x / SCALE / 16.0, y / SCALE / 16.0)
        };

        for x in -1..=base_x as i32 {
            for y in -1..=base_y as i32 {
                let (screen_x, screen_y) = (x as f32 * SCALE * 16.0 + shift_x as f32, y as f32 * SCALE * 16.0 + shift_y as f32);
                let x = x - base_x as i32 / 2 + player_coords.0 as i32;
                let y = y - base_y as i32 / 2 + player_coords.1 as i32;
                let mut sprite = Sprite::new();
                
                sprite.set_texture(self.world.get_block(x, y).block.texture(context.textures), true); // i dont care sfml just let me draw the texture
                sprite.set_position(Vector2f::new(screen_x, screen_y));
                sprite.set_scale(Vector2f::new(SCALE, SCALE));
                render_target.draw_sprite(&sprite, &RenderStates::default());
            }
        }

        {
            let mut sprite = Sprite::new();
            let tex_size = context.textures.player.size();
            let win_size = render_target.size();

            sprite.set_texture(&context.textures.player, true);
            sprite.set_position(Vector2f::new(tex_size.x as f32 / -2.0 * SCALE + win_size.x as f32 / 2.0, tex_size.y as f32 / -2.0 * SCALE + win_size.y as f32 / 2.0));
            sprite.set_scale(Vector2f::new(SCALE, SCALE));
            render_target.draw_sprite(&sprite, &RenderStates::default());
        }

        Ok(Flow::Continue)
    }
}
