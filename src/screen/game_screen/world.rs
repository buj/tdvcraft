use std::collections::HashMap;

use sfml::{SfBox, graphics::Texture};

use crate::textures::Textures;

pub const CHUNK_SIZE: usize = 32;

/// A type of entity
#[derive(PartialEq)]
pub enum EntityType {
    Player,
}

/// An entity
pub struct Entity {
    pub x: f32,
    pub y: f32,
    pub entity: EntityType,
}

/// A type of block
#[derive(PartialEq)]
pub enum BlockType {
    Dirt,
    Grass,
}

impl BlockType {
    pub fn texture<'a>(&'a self, texs: &'a Textures) -> &SfBox<Texture> {
        match self {
            BlockType::Dirt => &texs.dirt,
            BlockType::Grass => &texs.grass,
        }
    }
}

/// Block itself
pub struct Block {
    pub block: BlockType,
}

pub struct Chunk {
    pub blocks: Vec<Block>,
}

impl Chunk {
    pub fn new() -> Self {
        let mut blocks = Vec::with_capacity(CHUNK_SIZE * CHUNK_SIZE);

        for _ in 0..blocks.capacity() {
            blocks.push(Block {
                block: BlockType::Grass,
            });
        }

        Self {
            blocks,
        }
    }
}

pub struct World {
    pub entities: Vec<Entity>,
    chunks: HashMap<(i32, i32), Chunk>,
}

impl World {
    pub fn new() -> Self {
        Self {
            entities: Vec::new(),
            chunks: HashMap::new(),
        }
    }

    pub fn spawn(&mut self, entity: Entity) {
        self.entities.push(entity);
    }

    fn get_chunk(&mut self, x: i32, y: i32) -> &Chunk {
        if self.chunks.contains_key(&(x, y)) {
            self.chunks.get(&(x, y)).unwrap()
        } else {
            let chunk = Chunk::new();
            self.chunks.insert((x, y), chunk);
            self.chunks.get(&(x, y)).unwrap()
        }
    }

    fn get_chunk_mut(&mut self, x: i32, y: i32) -> &mut Chunk {
        if self.chunks.contains_key(&(x, y)) {
            self.chunks.get_mut(&(x, y)).unwrap()
        } else {
            let chunk = Chunk::new();
            self.chunks.insert((x, y), chunk);
            self.chunks.get_mut(&(x, y)).unwrap()
        }
    }

    pub fn get_block(&mut self, x: i32, y: i32) -> &Block {
        let bx = if x < 0 { CHUNK_SIZE as i32 + x % 16 } else { x % 16 } as usize;
        let by = if y < 0 { CHUNK_SIZE as i32 + y % 16 } else { y % 16 } as usize;
        &self.get_chunk(x / 16, y / 16).blocks[bx + by * CHUNK_SIZE]
    }

    pub fn get_block_mut(&mut self, x: i32, y: i32) -> &mut Block {
        let bx = if x < 0 { CHUNK_SIZE as i32 + x % 16 } else { x % 16 } as usize;
        let by = if y < 0 { CHUNK_SIZE as i32 + y % 16 } else { y % 16 } as usize;
        &mut self.get_chunk_mut(x / 16, y / 16).blocks[bx + by * CHUNK_SIZE]
    }
}
