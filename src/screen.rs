use std::time::{Duration, SystemTime};

use sfml::{window::Event, graphics::RenderWindow};

use crate::{Result, textures::Textures};

pub mod game_screen;

pub struct Context<'a> {
    pub delta: Duration,
    pub time: SystemTime,
    pub textures: &'a Textures,
}

/// A screen game is currently presenting to the player
pub trait Screen {
    fn handle_event(&mut self, event: &Event, context: Context) -> Result<Flow>;
    fn frame(&mut self, render_target: &mut RenderWindow, context: Context) -> Result<Flow>;
}

/// Flow of the application
pub enum Flow {
    /// Continue application as normally
    Continue,
    /// Exit application
    Quit,
    /// Switch screen
    NewScreen(Box<dyn Screen>),
}
