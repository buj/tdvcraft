pub mod screen;
pub mod textures;

use std::time::SystemTime;

use screen::{Screen, game_screen::GameScreen, Context};
use sfml::{graphics::RenderWindow, window::Style};
use textures::Textures;

/// An enum marking a function as failible
pub type Result<T, E = Box<dyn std::error::Error + Send + Sync + 'static>> = ::std::prelude::rust_2021::Result<T, E>;

/// The main function
fn main() -> Result<()> {
    // Window that is presented to the user
    let mut window = RenderWindow::new(
        (50 * 16, 37 * 16),
        "TDVCraft",
        Style::DEFAULT,
        &sfml::window::ContextSettings::default(),
    );
    // Enable vertical sync
    window.set_vertical_sync_enabled(true);

    // Screen that is presented to the user
    let mut screen: Box<dyn Screen> = Box::new(GameScreen::new());
    // Application time
    let mut time = SystemTime::now();
    let textures = Textures::new()?;

    // Main loop
    'running: while window.is_open() {
        // Time of current frame
        let now = SystemTime::now();

        // Pass events to the screen and handle flow accordingly
        while let Some(event) = window.poll_event() {
            match screen.handle_event(&event, Context {
                delta: time.elapsed()?,
                time: now,
                textures: &textures,
            })? {
                screen::Flow::Continue => (),
                screen::Flow::Quit => {
                    window.close();
                    break 'running;
                }
                screen::Flow::NewScreen(new_screen) => screen = new_screen,
            }
        }

        // Render the screen and handle the flow
        match screen.frame(&mut window, Context {
            delta: time.elapsed()?,
            time: now,
            textures: &textures,
        })? {
            screen::Flow::Continue => (),
            screen::Flow::Quit => {
                window.close();
                break;
            }
            screen::Flow::NewScreen(new_screen) => screen = new_screen,
        }

        // Present the window to the user
        window.display();

        time = now;
    }

    // Successfully exit the application
    Ok(())
}
