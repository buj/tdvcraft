use sfml::{graphics::{Texture, IntRect}, SfBox};

use crate::Result;


static TERRAIN_TEX: &[u8] = include_bytes!("../resources/terrain.png");
static PLAYER_TEX: &[u8] = include_bytes!("../resources/player.png");

pub struct Textures {
    pub dirt: SfBox<Texture>,
    pub grass: SfBox<Texture>,

    pub player: SfBox<Texture>,
}

impl Textures {
    pub fn new() -> Result<Self> {
        Ok(Self {
            dirt: {
                let mut texture = Texture::new().unwrap();
                texture.load_from_memory(TERRAIN_TEX, IntRect::new(0, 0, 16, 16))?;
                texture.set_smooth(false);
                texture
            },
            grass: {
                let mut texture = Texture::new().unwrap();
                texture.load_from_memory(TERRAIN_TEX, IntRect::new(16, 0, 16, 16))?;
                texture.set_smooth(false);
                texture
            },
            player: {
                let mut texture = Texture::new().unwrap();
                texture.load_from_memory(PLAYER_TEX, IntRect::new(0, 0, 16, 30))?;
                texture.set_smooth(false);
                texture
            },
        })
    }
}
